{pkgs ? import <nixpkgs> {}}:
with pkgs; let
  py-packs = pypac:
    with pypac; [
      pylsp-mypy
      python-decouple
      python-lsp-server
    ];
  my-python = python310.withPackages py-packs;
in
  mkShell {
    buildInputs = [
      # my-python
      python311

      mypy
      black
      pylint

      jq
      pipenv
    ];
  }
