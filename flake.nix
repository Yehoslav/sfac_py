{
  description = "S'Fac: un bot Telegram pentru asistarea la procrastinare.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
    in
      with pkgs; {
        packages = {
          default = stdenv.mkDerivation {
            name = "S'Fac";
            version = "0.0.1";
            src = builtins.path {
              path = ./.;
              name = "sfac_py";
            };
          };
        };

        formatter = alejandra;

        devShells.default = import ./shell.nix {inherit pkgs;};
      });
}
