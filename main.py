import sys
import os
import logging

import telebot as tel
import telebot.types as T


TOKEN = os.environ["TG_TOKEN"]
logger = tel.logger
logger.setLevel(logging.INFO)

bot = tel.TeleBot(TOKEN)

def main():
    bot.infinity_polling(allowed_updates=["message", "inline_query"])


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('\nExiting by user request.\n')
        sys.exit(0)
